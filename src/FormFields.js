import React , {useState} from "react";
import { useForm } from "react-hook-form";
import Identifier from "./Components/identifier";
import Titles from "./Components/titles";
import Creators from "./Components/creators";
import Publisher from "./Components/publisher";
import PublicationYear from "./Components/publicationYear";
import ResourceType from "./Components/resourceType";
import Subjects from "./Components/subjects";
import Contributors from "./Components/contributors";
import Dates from "./Components/dates";
import RelatedIdentifiers from "./Components/relatedIdentifiers";
import Descriptions from "./Components/descriptions";
import GeoLocations from "./Components/geoLocations";
import Language from "./Components/language";
import AlternateIdentifiers from "./Components/alternateIdentifiers";
import Sizes from "./Components/sizes";
import Formats from "./Components/formats";
import Version from "./Components/version";
import RightsList from "./Components/rightsList";
import FundingReferences from "./Components/fundingreferences"

export default ({ initialState, prev, next }) => {
    const { register, handleSubmit } = useForm({
        defaultValues: initialState
    });

    const onSubmit = values => {
        next(values);
    };

    const [showMandatory, setShowMandatory] = useState(false);
    const [showRecommended, setShowRecommended] = useState(false);
    const [showOther, setShowOther] = useState(false);

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <h2>Metadata Generator:</h2>


                <div>
                    <button type = "button" onClick={() => setShowMandatory(!showMandatory)}>
                        <h2> Add Mandatory Elements</h2>
                    </button>
                    {showMandatory &&
                        <div>
                    <Identifier register={register}  />
                    <Titles register={register}  />
                    <Creators register={register} />
                    <Publisher register={register} />
                    <PublicationYear register={register}/>
                    <ResourceType register={register}/>
                        </div>}
                </div>

                <div>
                    <button type="button" onClick={() => setShowRecommended(!showRecommended)}>
                        <h2>Add Recommended Elements</h2>
                    </button>
                    {showRecommended &&
                        <div>
                <Subjects register={register}/>
                <Contributors register={register}/>
                <Dates register={register}/>
                <RelatedIdentifiers register={register}/>
                <Descriptions register={register}/>
                <GeoLocations register={register}/>
                        </div>}
                </div>

                <div>
                    <button type="button" onClick={() => setShowOther(!showOther)}>
                <h2>Add Other Elements</h2>
                    </button>
                    {showOther &&
                        <div>
                <Language register={register}/>
                <AlternateIdentifiers register={register}/>
                <Sizes register={register} />
                <Formats register={register} />
                <Version register={register}/>
                <RightsList register={register}/>
                <FundingReferences register={register}/>
                        </div>}
                </div>
                <button type="button" className="appButton" onClick={prev} disabled={true}>
                    Previous
                </button>
                <button type="submit" className="appButton" >Next</button>
            </form>
        </>
    );
};