import React, { useState } from "react";
import './App.css';
import { saveAs } from "file-saver";
import FormFields from "./FormFields";
import Result from "./Result";

import { makeStyles } from "@material-ui/core/styles";
//import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme =>
    ({
      appBar: {
        position: "relative",
        color: "#4F6A94",

      },
      layout: {
        width: "auto",
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
          width: 600,
          marginLeft: "auto",
          marginRight: "auto",
        },
      },




    }));




export default function App() {
  const classes = useStyles();
  const [state, setState] = useState({
    globalForm: {},
    step: 0
  });
  const totalSteps = 1;

  const next = values => {
    setState(prevState => {
      const newGlobalForm = {
        ...prevState.globalForm,
        ...values
      };

      const newStep = Math.min(prevState.step + 1, totalSteps);

      return {
        globalForm: newGlobalForm,
        step: newStep
      };
    });
  };

  const prev = () => {
    setState(prevState => {
      return {
        ...prevState,
        step: Math.max(0, prevState.step - 1)
      };
    });
  };

  const convertXml = function() {
    // xml-js operations
    var convert = require("xml-js");
    var json = state.globalForm;
    var options = { compact: false, ignoreComment: true, spaces: 4 };
    var result = convert.json2xml(json, options);

    // xml remove empty tags and attributes:
    // - remove empty tags i.e. <givenName></givenName>
    // - remove empty string attributes i.e. <subject subjectScheme = "" ></subject
    // if in childNodes exist emptyChildNode -> deleteNode
    // if in nodes exist nodeAttribute=""? delete nodeAttribute : nodeAttribute



    // file saver operations
    var fileName = "myData.xml";
    var fileToSave = new Blob([result], {
      type: "application/xml",
      name: fileName
    });
    saveAs(fileToSave, fileName);
  };

  const convertJSON = function(state) {
    // saves myData.json file

    var fileName = "myData.json";
    var fileToSave = new Blob([JSON.stringify(state.globalForm)], {
      type: "application/json",
      name: fileName
    });
    saveAs(fileToSave, fileName);
  };



  console.log(state.globalForm);

  return (
      <div>
        <AppBar position="absolute" color="inherit" className={classes.appBar}>
          <Toolbar>
            <Typography variant="h4" color="inherit" noWrap>
              DataCite Metadata Generator Kernel 4.3
            </Typography>
          </Toolbar>
        </AppBar>
        {state.step === 0 && (
            <div>
              <FormFields initialState={state.globalForm} prev={prev} next={next} />
            </div>
        )}
        {state.step === 1 && (
            <div>
              <Result initialState={state.globalForm} prev={prev} convertJSON={convertJSON} convertXml={convertXml} />
            </div>
        )}
      </div>
  );
}

