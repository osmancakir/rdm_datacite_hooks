import React from "react";

export default function Language({register}) {
    return (
        <>

            <fieldset>
                <label> Language :
                    <input type="hidden" name="elements[0].elements[12].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[12].name" defaultValue="identifier" ref={register}/>
                    <input type="hidden" name="elements[0].elements[12].elements[0].type" defaultValue="text" ref={register}/>
                    <input className="language" name="elements[0].elements[12].elements[0].text" placeholder="Language" ref={register}/>
                </label>
            </fieldset>

        </>
    )
}