import React from "react";



export default function RelatedIdentifiers({register}) {

    // dynamic input

    const [counter, setCounter] = React.useState(1);
    const [relatedIdentifiers, setRelatedIdentifiers] = React.useState([0]);

    const addRelatedIdentifiersFieldset = () => {
        setRelatedIdentifiers(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };

    const removeRelatedIdentifiersFieldset = index => () => {
        setRelatedIdentifiers(prevIndexes => [
            ...prevIndexes.filter(item => item !== index)
        ]);
        setCounter(prevCounter => prevCounter - 1);
    };



    return(
        <>
            <fieldset>
                <label> Related Identifiers:
                    <input type="hidden" name="elements[0].elements[9].type"  defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[9].name" defaultValue="relatedIdentifiers" ref={register}/>

                    {relatedIdentifiers.map(index => {
                        const relatedIdentifiersFieldName = `elements[0].elements[9].elements[${index}]`;
                        return (
                            <fieldset name={relatedIdentifiersFieldName} key={relatedIdentifiersFieldName} >
                                <input type="hidden" name={`${relatedIdentifiersFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${relatedIdentifiersFieldName}.name`} defaultValue="relatedIdentifier" ref={register}/>

                                <label> relatedIdentifier:
                                    <input type="hidden" name={`${relatedIdentifiersFieldName}.elements[0].type`}  ref={register}/>
                                    <input  name={`${relatedIdentifiersFieldName}.elements[0].text`}  placeholder="Input" ref={register}/>
                                </label>

                                <label className ="halfwidth" > relatedIdentifierType:
                                    <select  name={`${relatedIdentifiersFieldName}.attributes.relatedIdentifierType`} ref={register}>
                                        <option value="">[relatedIdentifierType]</option>
                                        <option value="ARK">ARK</option>
                                        <option value="arXiv">arXiv</option>
                                        <option value="bibcode">bibcode</option>
                                        <option value="DOI">DOI</option>
                                        <option value="EAN13">EAN13</option>
                                        <option value="EISSN">EISSN</option>
                                        <option value="Handle">Handle</option>
                                        <option value="IGSN">IGSN</option>
                                        <option value="ISBN">ISBN</option>
                                        <option value="ISSN">ISSN</option>
                                        <option value="ISTC">ISTC</option>
                                        <option value="LISSN">LISSN</option>
                                        <option value="LSID">LSID</option>
                                        <option value="PMID">PMID</option>
                                        <option value="PURL">PURL</option>
                                        <option value="UPC">UPC</option>
                                        <option value="URL">URL</option>
                                        <option value="URN">URN</option>
                                    </select>
                                </label>

                                <label className ="halfwidth" > relationType:
                                    <select name={`${relatedIdentifiersFieldName}.attributes.relationType`} ref={register}>
                                        <option value="">[relationType]</option>
                                        <option value="Cites/IsCitedBy">Cites/IsCitedBy</option>
                                        <option value="Describes/IsDescribedBy">Describes/IsDescribedBy</option>
                                        <option value="HasPart/IsPartOf">HasPart/IsPartOf</option>
                                        <option value="HasMetadata">HasMetadata</option>
                                        <option value="HasVersion">HasVersion</option>
                                        <option value="EISSN">EISSN</option>
                                        <option value="IsVersionOf">IsVersionOf</option>
                                        <option value="IsNewVersionOf/IsPreviousVersionOf">IsNewVersionOf/IsPreviousVersionOf</option>
                                        <option value="IsCompiledBy/IsSourceOf">IsCompiledBy/IsSourceOf</option>
                                        <option value="References/IsReferencedBy">References/IsReferencedBy</option>
                                        <option value="IsVariantFormOf">IsVariantFormOf"</option>
                                        <option value="IsIdenticalTo">IsIdenticalTo</option>
                                        <option value="IsSupplemtentedBy/Supplements">IsSupplemtentedBy/Supplements</option>
                                        <option value="IsDocumentedBy/Documents">IsDocumentedBy/Documents</option>
                                    </select>
                                </label>

                                <label className ="halfwidth" > relatedMetadataScheme:
                                    <input name={`${relatedIdentifiersFieldName}.attributes.relatedMetadataScheme`} placeholder="Input" ref={register}/>
                                </label>

                                <label className ="halfwidth" > schemeType:
                                    <input name={`${relatedIdentifiersFieldName}.attributes.schemeType`} placeholder="Input" ref={register}/>
                                </label>


                                <button type="button" onClick={addRelatedIdentifiersFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeRelatedIdentifiersFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}
                </label>
            </fieldset>
        </>
    )
}