import React from "react";



export default function GeoLocations({register}) {

    // dynamic input Creators

    const [counter, setCounter] = React.useState(1);
    const [geoLocations, setGeoLocations] = React.useState([0]);

    const addGeoLocationsFieldset = () => {
        setGeoLocations(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };

    const removeGeoLocationsFieldset = index => () => {
        setGeoLocations(prevIndexes => [
            ...prevIndexes.filter(item => item !== index)
        ]);
        setCounter(prevCounter => prevCounter - 1);
    };



    return(
        <>
            <fieldset>
                <label> Geo Locations:
                    <input type="hidden" name="elements[0].elements[11].type"  defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[11].name" defaultValue="geoLocations" ref={register}/>

                    {geoLocations.map(index => {
                        const geoLocationsFieldName = `elements[0].elements[11].elements[${index}]`;
                        return (
                            <fieldset name={geoLocationsFieldName} key={geoLocationsFieldName} >
                                <label> Geo Location:  </label>
                                <input type="hidden" name={`${geoLocationsFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${geoLocationsFieldName}.name`} defaultValue="geoLocation" ref={register}/>


                                <label> geoLocationPlace:
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[0].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[0].name`} defaultValue="geoLocationPlace" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[0].elements[0].type`} defaultValue="text" ref={register}/>

                                    <input
                                        placeholder="geoLocationPlace"
                                        name={`${geoLocationsFieldName}.elements[0].elements[0].text`}
                                        ref={register}

                                    />
                                </label>

                                <fieldset>
                                    <label>geoLocationPoint:</label>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[1].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[1].name`} defaultValue="geoLocationPoint" ref={register}/>
                                <label className ="halfwidth" >
                                    pointLongitude:

                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[1].elements[0].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[1].elements[0].name`} defaultValue="pointLongitude" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[1].elements[0].elements[0].type`} defaultValue="text" ref={register}/>
                                    <input
                                        className="geoLocation"
                                        placeholder="pointLongitude"
                                        name={`${geoLocationsFieldName}.elements[1].elements[0].elements[0].text`}
                                        ref={register}
                                    />
                                </label>
                                <label className ="halfwidth" >
                                    pointLatitude:
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[1].elements[1].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[1].elements[1].name`} defaultValue="pointLongitude" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[1].elements[1].elements[0].type`} defaultValue="text" ref={register}/>
                                    <input
                                        className="geoLocation"
                                        placeholder="pointLatitude"
                                        name={`${geoLocationsFieldName}.elements[1].elements[1].elements[0].text`}
                                        ref={register}
                                    />
                                </label>
                                </fieldset>

                                <fieldset>
                                    <label>geoLocationBox:</label>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[2].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[2].name`} defaultValue="geoLocationBox" ref={register}/>
                                    <label className ="halfwidth" >
                                        westBoundLongitude:
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[0].type`} defaultValue="element" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[0].name`} defaultValue="westBoundLongitude" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[0].elements[0].type`} defaultValue="text" ref={register}/>
                                        <input
                                            className="geoLocation"
                                            placeholder="westBoundLongitude"
                                            name={`${geoLocationsFieldName}.elements[2].elements[0].elements[0].text`}
                                            ref={register}
                                        />
                                    </label>
                                    <label className ="halfwidth" >
                                        eastBoundLongitude:
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[1].type`} defaultValue="element" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[1].name`} defaultValue="eastBoundLongitude" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[1].elements[0].type`} defaultValue="text" ref={register}/>
                                        <input
                                            className="geoLocation"
                                            placeholder="eastBoundLongitude"
                                            name={`${geoLocationsFieldName}.elements[2].elements[1].elements[0].text`}
                                            ref={register}
                                        />
                                    </label>
                                    <label className ="halfwidth" >
                                        southBoundLatitude:
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[2].type`} defaultValue="element" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[2].name`} defaultValue="southBoundLatitude" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[2].elements[0].type`} defaultValue="text" ref={register}/>
                                        <input
                                            className="geoLocation"
                                            placeholder="southBoundLatitude"
                                            name={`${geoLocationsFieldName}.elements[2].elements[2].elements[0].text`}
                                            ref={register}
                                        />
                                    </label>
                                    <label className ="halfwidth" >
                                        northBoundLatitude:
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[3].type`} defaultValue="element" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[3].name`} defaultValue="northBoundLatitude" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[2].elements[3].elements[0].type`} defaultValue="text" ref={register}/>
                                        <input
                                            className="geoLocation"
                                            placeholder="northBoundLatitude"
                                            name={`${geoLocationsFieldName}.elements[2].elements[3].elements[0].text`}
                                            ref={register}
                                        />
                                    </label>
                                </fieldset>


                                <fieldset>
                                    <label>geoLocationPolygon:</label>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[3].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[3].name`} defaultValue="geoLocationPolygon" ref={register}/>

                                    <label>polygonPoint:</label>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[0].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[0].name`} defaultValue="polygonPoint" ref={register}/>
                                    <fieldset>
                                    <label className ="halfwidth" >
                                        pointLongitude
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[0].elements[0].type`} defaultValue="element" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[0].elements[0].name`} defaultValue="pointLongitude" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[0].elements[0].elements[0].type`} defaultValue="text" ref={register}/>
                                        <input
                                            className="geoLocation"
                                            placeholder="pointLongitude"
                                            name={`${geoLocationsFieldName}.elements[3].elements[0].elements[0].elements[0].text`}
                                            ref={register}
                                        />
                                    </label>
                                    <label className ="halfwidth" >
                                        pointLatitude
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[0].elements[1].type`} defaultValue="element" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[0].elements[1].name`} defaultValue="pointLatitude" ref={register}/>
                                        <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[0].elements[1].elements[0].type`} defaultValue="text" ref={register}/>
                                        <input
                                            className="geoLocation"
                                            placeholder="pointLatitude"
                                            name={`${geoLocationsFieldName}.elements[3].elements[0].elements[1].elements[0].text`}
                                            ref={register}
                                        />
                                    </label>
                                    </fieldset>

                                    <label>polygonPoint:</label>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[1].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[1].name`} defaultValue="polygonPoint" ref={register}/>
                                    <fieldset>
                                        <label className ="halfwidth" >
                                            pointLongitude
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[1].elements[0].type`} defaultValue="element" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[1].elements[0].name`} defaultValue="pointLongitude" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[1].elements[0].elements[0].type`} defaultValue="text" ref={register}/>
                                            <input
                                                className="geoLocation"
                                                placeholder="pointLongitude"
                                                name={`${geoLocationsFieldName}.elements[3].elements[1].elements[0].elements[0].text`}
                                                ref={register}
                                            />
                                        </label>
                                        <label className ="halfwidth" >
                                            pointLatitude
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[1].elements[1].type`} defaultValue="element" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[1].elements[1].name`} defaultValue="pointLatitude" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[1].elements[1].elements[0].type`} defaultValue="text" ref={register}/>
                                            <input
                                                className="geoLocation"
                                                placeholder="pointLatitude"
                                                name={`${geoLocationsFieldName}.elements[3].elements[1].elements[1].elements[0].text`}
                                                ref={register}
                                            />
                                        </label>
                                    </fieldset>


                                    <label>polygonPoint:</label>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[2].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[2].name`} defaultValue="polygonPoint" ref={register}/>
                                    <fieldset>
                                        <label className ="halfwidth" >
                                            pointLongitude
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[2].elements[0].type`} defaultValue="element" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[2].elements[0].name`} defaultValue="pointLongitude" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[2].elements[0].elements[0].type`} defaultValue="text" ref={register}/>
                                            <input
                                                className="geoLocation"
                                                placeholder="pointLongitude"
                                                name={`${geoLocationsFieldName}.elements[3].elements[2].elements[0].elements[0].text`}
                                                ref={register}
                                            />
                                        </label>
                                        <label className ="halfwidth" >
                                            pointLatitude
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[2].elements[1].type`} defaultValue="element" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[2].elements[1].name`} defaultValue="pointLatitude" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[2].elements[1].elements[0].type`} defaultValue="text" ref={register}/>
                                            <input
                                                className="geoLocation"
                                                placeholder="pointLatitude"
                                                name={`${geoLocationsFieldName}.elements[3].elements[2].elements[1].elements[0].text`}
                                                ref={register}
                                            />
                                        </label>
                                    </fieldset>


                                    <label>polygonPoint:</label>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[3].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[3].name`} defaultValue="polygonPoint" ref={register}/>
                                    <fieldset>
                                        <label className ="halfwidth">
                                            pointLongitude
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[3].elements[0].type`} defaultValue="element" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[3].elements[0].name`} defaultValue="pointLongitude" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[3].elements[0].elements[0].type`} defaultValue="text" ref={register}/>
                                            <input
                                                className="geoLocation"
                                                placeholder="pointLongitude"
                                                name={`${geoLocationsFieldName}.elements[3].elements[3].elements[0].elements[0].text`}
                                                ref={register}
                                            />
                                        </label>
                                        <label className ="halfwidth" >
                                            pointLatitude
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[3].elements[1].type`} defaultValue="element" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[3].elements[1].name`} defaultValue="pointLatitude" ref={register}/>
                                            <input type="hidden" name={`${geoLocationsFieldName}.elements[3].elements[3].elements[1].elements[0].type`} defaultValue="text" ref={register}/>
                                            <input
                                                className="geoLocation"
                                                placeholder="pointLatitude"
                                                name={`${geoLocationsFieldName}.elements[3].elements[3].elements[1].elements[0].text`}
                                                ref={register}
                                            />
                                        </label>
                                    </fieldset>

                                </fieldset>

                                <button type="button" onClick={addGeoLocationsFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeGeoLocationsFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}
                </label>
            </fieldset>
        </>
    )
}