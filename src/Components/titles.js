import React from "react";



import { withStyles } from '@material-ui/core/styles';

import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

const HtmlTooltip = withStyles(theme => ({
    tooltip: {
        backgroundColor: '#f5f5f9',
        color: 'rgba(0, 0, 0, 0.87)',
        maxWidth: 220,
        fontSize: theme.typography.pxToRem(12),
        border: '1px solid #dadde9',
    },
}))(Tooltip);


export default function Titles({register}){


    const [titles, setTitles] = React.useState([0]);
    const [counter, setCounter] = React.useState(1);

    const addTitlesFieldset = () => {
        setTitles(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };


    const removeTitlesFieldset = index => () => {
        setTitles(prevIndexes => [...prevIndexes.filter(item => item !== index)]); // for every item we want the items that does not equal to index
        setCounter(prevCounter => prevCounter - 1);
    };
    return (
        <>
        <HtmlTooltip placement="right-start" title={
            <React.Fragment>
                <Typography color="inherit">Titles: </Typography>
                <em>{"Title Documentation"}</em> <b>{'it is mandatory according to the DataCite standard'}</b> <u>{'Example:'}</u>.{' '}
                {""} {' '}

            </React.Fragment>
        }>
            <fieldset>

                <label> Titles:
                    <input type="hidden" name="elements[0].elements[1].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[1].name" defaultValue="titles" ref={register}/>
                    {titles.map(index => {
                        const fieldName = `elements[0].elements[1]elements[${index}]`;
                        return (
                            <fieldset name={fieldName} key={fieldName} >
                                <input type="hidden" name={`${fieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${fieldName}.name`} defaultValue="title" ref={register}/>
                                <label>Title:
                                    <input type="hidden" name={`${fieldName}.elements[0].type`} defaultValue="text" ref={register}/>
                                    <input name={`${fieldName}.elements[0].text`} placeholder="Input" ref={register}/>
                                </label>
                                <label className="halfwidth">
                                    Language:
                                    <input
                                        placeholder="Input"
                                        name={`${fieldName}.attributes.xml:lang`}
                                        ref={register}
                                    />
                                </label>
                                <label className="halfwidth" >Title Type :
                                    <select name={`${fieldName}.attributes.titleType`} ref={register}>
                                        <option value="">[titleType]</option>
                                        <option value="AlternativeTitle">AlternativeTitle</option>
                                        <option value="Subtitle">Subtitle</option>
                                        <option value="TranslatedTitle">TranslatedTitle</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </label>

                                <button type="button" onClick={addTitlesFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeTitlesFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}

                </label>
            </fieldset>
        </HtmlTooltip>
        </>
    )
}