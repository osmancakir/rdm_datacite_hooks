import React from "react";



export default function Contributors({register}) {


    const [counter, setCounter] = React.useState(1);
    const [contributors, setContributors] = React.useState([0]);

    const addContributorsFieldset = () => {
        setContributors(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };

    const removeContributorsFieldset = index => () => {
        setContributors(prevIndexes => [
            ...prevIndexes.filter(item => item !== index)
        ]);
        setCounter(prevCounter => prevCounter - 1);
    };



    return(
        <>
            <fieldset>
                <label> Contributors:
                    <input type="hidden" name="elements[0].elements[7].type"  defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[7].name" defaultValue="contributors" ref={register}/>

                    {contributors.map(index => {
                        const contributorsFieldName = `elements[0].elements[7].elements[${index}]`;
                        return (
                            <fieldset name={contributorsFieldName} key={contributorsFieldName} >
                                <input type="hidden" name={`${contributorsFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${contributorsFieldName}.name`} defaultValue="contributor" ref={register}/>
                                <input type="hidden" name={`${contributorsFieldName}.elements[0].type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${contributorsFieldName}.elements[0].name`} defaultValue="contributorName" ref={register}/>
                                <input type="hidden" name={`${contributorsFieldName}.elements[0].elements[0].type`} defaultValue="text" ref={register}/>

                                <label className="halfwidth">
                                    Contributor Name:
                                    <input
                                        placeholder="Input"
                                        name={`${contributorsFieldName}.elements[0].elements[0].text`}
                                        ref={register}

                                    />
                                </label>
                                <label className="halfwidth">
                                    contributorType:
                                    <select
                                        name={`${contributorsFieldName}.elements[0].attributes.contributorType`}
                                        ref={register}
                                        placeholder="Input"
                                    >
                                        <option value="">[contributorType]</option>
                                        <option value="DataCollector">DataCollector</option>
                                        <option value="DataCurator">DataCurator</option>
                                        <option value="HostingInstitution">HostingInstitution</option>
                                        <option value="ProjectLeader">ProjectLeader</option>
                                        <option value="ProjectManager">ProjectManager</option>
                                        <option value="ProjectMember">ProjectMember</option>
                                        <option value="Researcher">Researcher</option>
                                        <option value="RightsHolder">RightsHolder</option>
                                        <option value="WorkPackageLeader">WorkPackageLeader</option>
                                    </select>

                                </label>
                                <label className="halfwidth">
                                    givenName:
                                    <input type="hidden" name={`${contributorsFieldName}.elements[1].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${contributorsFieldName}.elements[1].name`} defaultValue="givenName" ref={register}/>
                                    <input type="hidden" name={`${contributorsFieldName}.elements[1].elements[0].type`} defaultValue="text" ref={register}/>
                                    <input
                                        placeholder="Input"
                                        name={`${contributorsFieldName}.elements[1].elements[0].text`}
                                        ref={register}
                                    />
                                </label>
                                <label className ="halfwidth">
                                    familyName:
                                    <input type="hidden" name={`${contributorsFieldName}.elements[2].type`} defaultValue="element"  ref={register}/>
                                    <input type="hidden" name={`${contributorsFieldName}.elements[2].name`} defaultValue="familyName"  ref={register}/>
                                    <input type="hidden" name={`${contributorsFieldName}.elements[2].elements[0].type`} defaultValue="text"  ref={register}/>
                                    <input
                                        placeholder="Input"
                                        name={`${contributorsFieldName}.elements[2].elements[0].text`}
                                        ref={register}
                                    />
                                </label>
                                <fieldset>
                                    <label className ="halfwidth" >
                                        {/* nested dynamic input here*/}
                                        nameIdentifier:
                                        <input type="hidden" name={`${contributorsFieldName}.elements[3].type`} defaultValue="element"  ref={register}/>
                                        <input type="hidden" name={`${contributorsFieldName}.elements[3].name`} defaultValue="nameIdentifier" ref={register}/>
                                        <input type="hidden" name={`${contributorsFieldName}.elements[3].elements[0].type`} defaultValue="text" ref={register}/>
                                        <input
                                            placeholder="Input"
                                            name={`${contributorsFieldName}.elements[3].elements[0].text`}
                                            ref={register}
                                        />
                                    </label>
                                    <label className ="halfwidth">
                                        nameIdentifierScheme:
                                        <select
                                            name={`${contributorsFieldName}.elements[3].attributes.nameIdentifierScheme`}
                                            ref={register}
                                        >
                                            <option value="">[nameIdentifierScheme]</option>
                                            <option value="ORCID">ORCID</option>
                                            <option value="GND">GND</option>
                                        </select>
                                    </label>
                                    <label>
                                        schemeURI:
                                        <input
                                            name={`${contributorsFieldName}.elements[3].attributes.schemeURI`}
                                            ref={register}
                                            placeholder="Input"
                                        />
                                    </label>
                                </fieldset>
                                <fieldset>
                                    <label>
                                        {/* nested dynamic input here*/}
                                        affiliation:
                                        <input type="hidden" name={`${contributorsFieldName}.elements[4].type`} defaultValue="element"  ref={register}/>
                                        <input type="hidden" name={`${contributorsFieldName}.elements[4].name`} defaultValue="affiliation" ref={register}/>
                                        <input type="hidden" name={`${contributorsFieldName}.elements[4].elements[0].type`} defaultValue="text" ref={register}/>
                                        <input
                                            name={`${contributorsFieldName}.elements[4].elements[0].text`}
                                            ref={register}
                                            placeholder="Input"
                                        />
                                    </label>
                                    <label> language:
                                        <input className="language" name = {`${contributorsFieldName}.elements[4].attributes.xml:lang`} placeholder="Input" ref={register}/>
                                    </label>
                                </fieldset>

                                <button type="button" onClick={addContributorsFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeContributorsFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}
                </label>
            </fieldset>
        </>
    )
}