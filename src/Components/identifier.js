import React from "react";


import { withStyles } from '@material-ui/core/styles';

import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';

const HtmlTooltip = withStyles(theme => ({
    tooltip: {
        backgroundColor: '#f5f5f9',
        color: 'rgba(0, 0, 0, 0.87)',
        maxWidth: 220,
        fontSize: theme.typography.pxToRem(12),
        border: '1px solid #dadde9',
    },
}))(Tooltip);

export default function Identifier({register}) {
    return (
        <>
            {/* XML headers */}
            <input type= "hidden" name="declaration.attributes.version" defaultValue="1.0" ref={register} />
            <input type= "hidden" name="declaration.attributes.encoding" defaultValue="utf-8" ref={register} />
            <input type="hidden" name="elements[0].type" defaultValue="element" ref={register}/>
            <input type="hidden" name="elements[0].name" defaultValue="resource" ref={register}/>
            <input type="hidden" name="elements[0].attributes.xmlns" defaultValue="https://schema.datacite.org/meta/kernel-4.3/" ref={register}/>
            <input type="hidden" name="elements[0].attributes.xml:xsi" defaultValue="http://www.w3.org/2001/XMLSchema-instance" ref={register}/>
            <input type="hidden" name="elements[0].attributes.xsi:schemaLocation" defaultValue="https://schema.datacite.org/meta/kernel-4.3/ https://schema.datacite.org/meta/kernel-4.3/metadata.xsd" ref={register}/>

            <HtmlTooltip placement="right-start" title={
                <React.Fragment>
                    <Typography color="inherit">Identifier: </Typography>
                    <em>{"This field can be omitted on submission"}</em> <b>{'it is mandatory according to the DataCite standard'}</b> <u>{'Example:'}</u>.{' '}
                    {"<identifier identifierType=\"DOI\">10.5282/ubm/data.158</identifier>"} {' '}

                </React.Fragment>
            }>
                        <fieldset>
                            <label> Identifier :
                                <input type="hidden" name="elements[0].elements[0].type" defaultValue="element" ref={register}/>
                                <input type="hidden" name="elements[0].elements[0].name" defaultValue="identifier" ref={register}/>
                                <input type="hidden" name="elements[0].elements[0].attributes.identifierType" defaultValue="DOI" ref={register}/>
                                <input type="hidden" name="elements[0].elements[0].elements[0].type" defaultValue="text" ref={register}/>
                                <input name="elements[0].elements[0].elements[0].text" placeholder="DOI" ref={register}/>
                            </label>
                        </fieldset>
            </HtmlTooltip>
        </>
    )
}