import React from "react";


export default function AlternateIdentifiers({register}){


    const [alternateIdentifiers, setAlternateIdentifiers] = React.useState([0]);
    const [counter, setCounter] = React.useState(1);

    const addAlternateIdentifiersFieldset = () => {
        setAlternateIdentifiers(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };


    const removeAlternateIdentifiersFieldset = index => () => {
        setAlternateIdentifiers(prevIndexes => [...prevIndexes.filter(item => item !== index)]);
        setCounter(prevCounter => prevCounter - 1);
    }
    return (
        <>
            <fieldset>
                <label> Alternate Identifiers:
                    <input type="hidden" name="elements[0].elements[13].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[13].name" defaultValue="alternateIdentifiers" ref={register}/>
                    {alternateIdentifiers.map(index => {
                        const alternateIdentifiersFieldName = `elements[0].elements[13]elements[${index}]`;
                        return (
                            <fieldset name={alternateIdentifiersFieldName} key={alternateIdentifiersFieldName} >
                                <input type="hidden" name={`${alternateIdentifiersFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${alternateIdentifiersFieldName}.name`} defaultValue="alternateIdentifier" ref={register}/>
                                <label>Alternate Identifier:
                                    <input type="hidden" name={`${alternateIdentifiersFieldName}.elements[0].type`} defaultValue="text" ref={register}/>
                                    <input name={`${alternateIdentifiersFieldName}.elements[0].text`} placeholder="Input" ref={register}/>
                                </label>
                                <label>
                                    Language:
                                    <input className="language"
                                           placeholder="Input"
                                           name={`${alternateIdentifiersFieldName}.attributes.alternateIdentifierType`}
                                           ref={register}
                                    />
                                </label>

                                <button type="button" onClick={addAlternateIdentifiersFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeAlternateIdentifiersFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}

                </label>
            </fieldset>

        </>
    )
}