import React from "react";
// dynamic input Titles





export default function Descriptions({register}){


    const [descriptions, setDescriptions] = React.useState([0]);
    const [counter, setCounter] = React.useState(1);

    const addDescriptionsFieldset = () => {
        setDescriptions(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };


    const removeDescriptionsFieldset = index => () => {
        setDescriptions(prevIndexes => [...prevIndexes.filter(item => item !== index)]);
        setCounter(prevCounter => prevCounter - 1);
    }
    return (
        <>
            <fieldset>
                <label> Descriptions:
                    <input type="hidden" name="elements[0].elements[10].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[10].name" defaultValue="descriptions" ref={register}/>
                    {descriptions.map(index => {
                        const descriptionsFieldName = `elements[0].elements[10]elements[${index}]`;
                        return (
                            <fieldset name={descriptionsFieldName} key={descriptionsFieldName} >
                                <input type="hidden" name={`${descriptionsFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${descriptionsFieldName}.name`} defaultValue="description" ref={register}/>

                                <label>Description:
                                    <input type="hidden" name={`${descriptionsFieldName}.elements[0].type`} defaultValue="text" ref={register}/>
                                    <textarea rows="4" cols="50" name={`${descriptionsFieldName}.elements[0].text`} placeholder="Input" ref={register}/>
                                </label>

                                <label>Description Language:
                                    <input className="language" name={`${descriptionsFieldName}.attributes.xml:lang`} placeholder="Input" ref={register}/>

                                </label>

                                <label>Description Type :
                                    <select name={`${descriptionsFieldName}.attributes.descriptionType`} ref={register}>
                                        <option value="">[descriptionType]</option>
                                        <option value="Abstract">Accepted</option>
                                        <option value="Methods">Available</option>
                                        <option value="TechnicalInfo">Copyrighted</option>
                                    </select>
                                </label>

                                <button type="button" onClick={addDescriptionsFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeDescriptionsFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}

                </label>
            </fieldset>

        </>
    )
}