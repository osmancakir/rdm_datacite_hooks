import React from "react";



export default function FundingReferences({register}) {

    // dynamic input Creators

    const [counter, setCounter] = React.useState(1);
    const [fundingReferences, setFundingReferences] = React.useState([0]);

    const addFundingReferencesFieldset = () => {
        setFundingReferences(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };

    const removeFundingReferencesFieldset = index => () => {
        setFundingReferences(prevIndexes => [
            ...prevIndexes.filter(item => item !== index)
        ]);
        setCounter(prevCounter => prevCounter - 1);
    };



    return(
        <>
            <fieldset>
                <label> Funding References:
                    <input type="hidden" name="elements[0].elements[18].type"  defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[18].name" defaultValue="fundingReferences" ref={register}/>

                    {fundingReferences.map(index => {
                        const fundingReferencesFieldName = `elements[0].elements[18].elements[${index}]`;
                        return (
                            <fieldset name={fundingReferencesFieldName} key={fundingReferencesFieldName} >
                                <input type="hidden" name={`${fundingReferencesFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${fundingReferencesFieldName}.name`} defaultValue="fundingReference" ref={register}/>
                                <input type="hidden" name={`${fundingReferencesFieldName}.elements[0].type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${fundingReferencesFieldName}.elements[0].name`} defaultValue="funderName" ref={register}/>
                                <input type="hidden" name={`${fundingReferencesFieldName}.elements[0].elements[0].type`} defaultValue="text" ref={register}/>

                                <label>
                                    Funder Name:

                                    <input
                                        placeholder="Input"
                                        name={`${fundingReferencesFieldName}.elements[0].elements[0].text`}
                                        ref={register}

                                    />
                                </label>

                                <label className ="halfwidth" >
                                    funderIdentifier:
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[1].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[1].name`} defaultValue="funderIdentifier" ref={register}/>
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[1].elements[0].type`} defaultValue="text" ref={register}/>
                                    <input
                                        placeholder="Input"
                                        name={`${fundingReferencesFieldName}.elements[1].elements[0].text`}
                                        ref={register}
                                    />
                                </label>
                                <label className ="halfwidth"  >
                                    funderIdentifierType:
                                    <select
                                        name={`${fundingReferencesFieldName}.elements[1].attributes.funderIdentifierType`}
                                        ref={register}
                                        placeholder="Input"
                                    >
                                        <option value="">[funderIdentifierType]</option>
                                        <option value="cordis">cordis</option>
                                        <option value="DFG">DFG</option>
                                        <option value="FWF">FWF</option>
                                    </select>

                                </label>

                                <label className ="halfwidth"  >
                                    awardNumber:
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[2].type`} defaultValue="element"  ref={register}/>
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[2].name`} defaultValue="awardNumber"  ref={register}/>
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[2].elements[0].type`} defaultValue="text"  ref={register}/>
                                    <input
                                        placeholder="Input"
                                        name={`${fundingReferencesFieldName}.elements[2].elements[0].text`}
                                        ref={register}
                                    />
                                </label>
                                <label className ="halfwidth"  >
                                    awardURI:
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[3].type`} defaultValue="element"  ref={register}/>
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[3].name`} defaultValue="awardURI" ref={register}/>
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[3].elements[0].type`} defaultValue="text" ref={register}/>
                                    <input
                                        placeholder="Input"
                                        name={`${fundingReferencesFieldName}.elements[3].elements[0].text`}
                                        ref={register}
                                    />
                                </label>
                                <label>
                                    awardTitle:
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[4].type`} defaultValue="element"  ref={register}/>
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[4].name`} defaultValue="awardTitle" ref={register}/>
                                    <input type="hidden" name={`${fundingReferencesFieldName}.elements[4].elements[0].type`} defaultValue="text" ref={register}/>
                                    <input
                                        placeholder="Input"
                                        name={`${fundingReferencesFieldName}.elements[4].elements[0].text`}
                                        ref={register}
                                    />
                                </label>
                                <label>
                                    Language:
                                    <input
                                        className="language"
                                        name={`${fundingReferencesFieldName}.elements[4].attributes.xml:lang`}
                                        ref={register}
                                        placeholder="Input"
                                    />
                                </label>


                                <button type="button" onClick={addFundingReferencesFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeFundingReferencesFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}
                </label>
            </fieldset>
        </>
    )
}