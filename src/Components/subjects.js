import React from "react";



export default function Subjects({register}) {

    // dynamic input Creators

    const [counter, setCounter] = React.useState(1);
    const [subjects, setSubjects] = React.useState([0]);

    const addSubjectsFieldset = () => {
        setSubjects(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };

    const removeSubjectsFieldset = index => () => {
        setSubjects(prevIndexes => [
            ...prevIndexes.filter(item => item !== index)
        ]);
        setCounter(prevCounter => prevCounter - 1);
    };



    return(
        <>
            <fieldset>
                <label> Subjects:
                    <input type="hidden" name="elements[0].elements[6].type"  defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[6].name" defaultValue="subjects" ref={register}/>

                    {subjects.map(index => {
                        const subjectsFieldName = `elements[0].elements[6].elements[${index}]`;
                        return (
                            <fieldset name={subjectsFieldName} key={subjectsFieldName} >
                                <input type="hidden" name={`${subjectsFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${subjectsFieldName}.name`} defaultValue="subject" ref={register}/>

                                <label className="halfwidth"> Subject:
                                <input type="hidden" name={`${subjectsFieldName}.elements[0].type`}  ref={register}/>
                                <input  name={`${subjectsFieldName}.elements[0].text`}  placeholder="Input" ref={register}/>
                                </label>

                                <label className="halfwidth"> subjectScheme:
                                    <select  name={`${subjectsFieldName}.attributes.subjectScheme`} ref={register}>
                                        <option value="">[subjectScheme]</option>
                                        <option value="GND">GND</option>
                                        <option value="wikidata">wikidata</option>
                                        <option value="dewey">dewey</option>
                                    </select>
                                </label>

                                <label className="halfwidth"> schemeURI:
                                    <input name={`${subjectsFieldName}.attributes.schemeURI`} placeholder="Input" ref={register}/>
                                </label>

                                <label className="halfwidth"> valueURI:
                                    <input name={`${subjectsFieldName}.attributes.valueURI`} placeholder="Input" ref={register}/>
                                </label>

                                <label> Language:
                                <input className="language" name={`${subjectsFieldName}.attributes.xml:lang`} placeholder="Input"  ref={register}/>
                                </label>

                                <button type="button" onClick={addSubjectsFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeSubjectsFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}
                </label>
            </fieldset>
        </>
    )
}