import React from "react";
// dynamic input Titles





export default function Dates({register}){


    const [dates, setDates] = React.useState([0]);
    const [counter, setCounter] = React.useState(1);

    const addDatesFieldset = () => {
        setDates(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };


    const removeDatesFieldset = index => () => {
        setDates(prevIndexes => [...prevIndexes.filter(item => item !== index)]);
        setCounter(prevCounter => prevCounter - 1);
    }
    return (
        <>
            <fieldset>
                <label> Dates:
                    <input type="hidden" name="elements[0].elements[8].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[8].name" defaultValue="dates" ref={register}/>
                    {dates.map(index => {
                        const datesFieldName = `elements[0].elements[8]elements[${index}]`;
                        return (
                            <fieldset name={datesFieldName} key={datesFieldName} >
                                <input type="hidden" name={`${datesFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${datesFieldName}.name`} defaultValue="date" ref={register}/>

                                <label className ="halfwidth" >Date:
                                    <input type="hidden" name={`${datesFieldName}.elements[0].type`} defaultValue="text" ref={register}/>
                                    <input type="datetime-local" name={`${datesFieldName}.elements[0].text`} placeholder="DD/MM/YYYY" ref={register}/>
                                </label>

                                <label className ="halfwidth" >Date Type :
                                    <select name={`${datesFieldName}.attributes.dateType`} ref={register}>
                                        <option value="">[dateType]</option>
                                        <option value="Accepted">Accepted</option>
                                        <option value="Available">Available</option>
                                        <option value="Copyrighted">Copyrighted</option>
                                        <option value="Collected">Collected</option>
                                        <option value="Created">Created</option>
                                        <option value="Issued">Issued</option>
                                        <option value="Submitted">Submitted</option>
                                        <option value="Updated">Updated</option>
                                        <option value="Valid">Valid</option>
                                        <option value="Withdrawn">Withdrawn</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </label>

                                <button type="button" onClick={addDatesFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeDatesFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}

                </label>
            </fieldset>

        </>
    )
}