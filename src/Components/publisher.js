
import React from "react";

export default function Publisher({register}) {
    return (
        <>

            <fieldset>
                <label> Publisher :
                    <input type="hidden" name="elements[0].elements[3].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[3].name" defaultValue="publisher" ref={register}/>
                    <label>Publisher:
                        <input type="hidden" name="elements[0].elements[3].elements[0].type" defaultValue="text" ref={register}/>
                        <input name="elements[0].elements[3].elements[0].text" placeholder="Input" ref={register}/>
                    </label>
                    <label> Language:
                    <input className="language" name="elements[0].elements[3].attributes.xml:lang" placeholder="Input" ref={register}/>
                    </label>
                </label>
            </fieldset>

        </>
    )
}