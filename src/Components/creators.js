import React from "react";



export default function Creators({register}) {

    // dynamic input Creators

    const [counter, setCounter] = React.useState(1);
    const [creators, setCreators] = React.useState([0]);

    const addCreatorsFieldset = () => {
        setCreators(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };

    const removeCreatorsFieldset = index => () => {
        setCreators(prevIndexes => [
            ...prevIndexes.filter(item => item !== index)
        ]);
        setCounter(prevCounter => prevCounter - 1);
    };



    return(
        <>
            <fieldset>
                <label> Creators:
                    <input type="hidden" name="elements[0].elements[2].type"  defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[2].name" defaultValue="creators" ref={register}/>

                    {creators.map(index => {
                        const creatorsFieldName = `elements[0].elements[2].elements[${index}]`;
                        return (
                            <fieldset name={creatorsFieldName} key={creatorsFieldName} >
                                <input type="hidden" name={`${creatorsFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${creatorsFieldName}.name`} defaultValue="creator" ref={register}/>
                                <input type="hidden" name={`${creatorsFieldName}.elements[0].type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${creatorsFieldName}.elements[0].name`} defaultValue="creatorName" ref={register}/>
                                <input type="hidden" name={`${creatorsFieldName}.elements[0].elements[0].type`} defaultValue="text" ref={register}/>

                                <label className="halfwidth">
                                    Creator Name:

                                    <input
                                        placeholder="Input"
                                        name={`${creatorsFieldName}.elements[0].elements[0].text`}
                                        ref={register}

                                    />
                                </label>
                                <label className="halfwidth">
                                    nameType:
                                    <select
                                        name={`${creatorsFieldName}.elements[0].attributes.nameType`}
                                        ref={register}
                                        placeholder="Input"
                                    >
                                        <option value="">[nameType]</option>
                                        <option value="personal">Personal</option>
                                        <option value="organizational">Organizational</option>
                                    </select>

                                </label>
                                <label className="halfwidth">
                                    givenName:
                                    <input type="hidden" name={`${creatorsFieldName}.elements[1].type`} defaultValue="element" ref={register}/>
                                    <input type="hidden" name={`${creatorsFieldName}.elements[1].name`} defaultValue="givenName" ref={register}/>
                                    <input type="hidden" name={`${creatorsFieldName}.elements[1].elements[0].type`} defaultValue="text" ref={register}/>
                                    <input
                                        placeholder="Input"
                                        name={`${creatorsFieldName}.elements[1].elements[0].text`}
                                        ref={register}
                                    />
                                </label>
                                <label className="halfwidth" >
                                    familyName:
                                    <input type="hidden" name={`${creatorsFieldName}.elements[2].type`} defaultValue="element"  ref={register}/>
                                    <input type="hidden" name={`${creatorsFieldName}.elements[2].name`} defaultValue="familyName"  ref={register}/>
                                    <input type="hidden" name={`${creatorsFieldName}.elements[2].elements[0].type`} defaultValue="text"  ref={register}/>
                                    <input
                                        placeholder="Input"
                                        name={`${creatorsFieldName}.elements[2].elements[0].text`}
                                        ref={register}
                                    />
                                </label>
                                <fieldset>
                                    <label>
                                        {/* nested dynamic input here*/}
                                        nameIdentifier:
                                        <input type="hidden" name={`${creatorsFieldName}.elements[3].type`} defaultValue="element"  ref={register}/>
                                        <input type="hidden" name={`${creatorsFieldName}.elements[3].name`} defaultValue="nameIdentifier" ref={register}/>
                                        <input type="hidden" name={`${creatorsFieldName}.elements[3].elements[0].type`} defaultValue="text" ref={register}/>
                                        <input
                                            placeholder="Input"
                                            name={`${creatorsFieldName}.elements[3].elements[0].text`}
                                            ref={register}
                                        />
                                    </label>
                                    <label className="halfwidth" >
                                        nameIdentifierScheme:
                                        <select
                                            name={`${creatorsFieldName}.elements[3].attributes.nameIdentifierScheme`}
                                            ref={register}
                                        >
                                            <option value="">[nameIdentifierScheme]</option>
                                            <option value="ORCID">ORCID</option>
                                            <option value="GND">GND</option>
                                        </select>
                                    </label>
                                    <label className="halfwidth" >
                                        schemeURI:
                                        <input
                                            name={`${creatorsFieldName}.elements[3].attributes.schemeURI`}
                                            ref={register}
                                            placeholder="Input"
                                        />
                                    </label>
                                </fieldset>
                                <fieldset>
                                    <label>
                                        {/* nested dynamic input here*/}
                                        affiliation:
                                        <input type="hidden" name={`${creatorsFieldName}.elements[4].type`} defaultValue="element"  ref={register}/>
                                        <input type="hidden" name={`${creatorsFieldName}.elements[4].name`} defaultValue="affiliation" ref={register}/>
                                        <input type="hidden" name={`${creatorsFieldName}.elements[4].elements[0].type`} defaultValue="text" ref={register}/>
                                        <input
                                            name={`${creatorsFieldName}.elements[4].elements[0].text`}
                                            ref={register}
                                            placeholder="Input"
                                        />
                                    </label>
                                    <label> affiliation language:
                                        <input className="language" name = {`${creatorsFieldName}.elements[4].attributes.xml:lang`} placeholder="Input" ref={register}/>
                                    </label>
                                </fieldset>

                                <button type="button" onClick={addCreatorsFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeCreatorsFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}
                </label>
            </fieldset>
        </>
    )
}