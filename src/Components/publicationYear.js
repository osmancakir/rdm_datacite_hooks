
import React from "react";

export default function PublicationYear({register}) {
    return (
        <>

            <fieldset>
                <label> Publication Year :
                    <input type="hidden" name="elements[0].elements[4].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[4].name" defaultValue="publicationYear" ref={register}/>
                    <input type="hidden" name="elements[0].elements[4].elements[0].type" defaultValue="text" ref={register}/>
                    <input name="elements[0].elements[4].elements[0].text" placeholder="YYYY" ref={register}/>
                </label>
            </fieldset>

        </>
    )
}