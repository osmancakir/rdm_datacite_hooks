import React from "react";






export default function Formats({register}){


    const [formats, setFormats] = React.useState([0]);
    const [counter, setCounter] = React.useState(1);

    const addFormatsFieldset = () => {
        setFormats(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };


    const removeFormatsFieldset = index => () => {
        setFormats(prevIndexes => [...prevIndexes.filter(item => item !== index)]);
        setCounter(prevCounter => prevCounter - 1);
    }
    return (
        <>
            <fieldset>
                <label> Formats:
                    <input type="hidden" name="elements[0].elements[15].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[15].name" defaultValue="sizes" ref={register}/>
                    {formats.map(index => {
                        const formatsFieldName = `elements[0].elements[15]elements[${index}]`;
                        return (
                            <fieldset name={formatsFieldName} key={formatsFieldName} >
                                <input type="hidden" name={`${formatsFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${formatsFieldName}.name`} defaultValue="formats" ref={register}/>
                                <label>Format:
                                    <input type="hidden" name={`${formatsFieldName}.elements[0].type`} defaultValue="text" ref={register}/>
                                    <input name={`${formatsFieldName}.elements[0].text`} placeholder="Input" ref={register}/>
                                </label>

                                <button type="button" onClick={addFormatsFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeFormatsFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}

                </label>
            </fieldset>

        </>
    )
}