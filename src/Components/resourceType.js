
import React from "react";

export default function ResourceType({register}) {
    return (
        <>

            <fieldset>
                <label> Resource Type:
                    <input type="hidden" name="elements[0].elements[5].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[5].name" defaultValue="resourceType" ref={register}/>

                    <label>
                        Resource Type:
                        <input type="hidden" name="elements[0].elements[5].elements[0].type" defaultValue="text" ref={register}/>
                        <input name="elements[0].elements[5].elements[0].text" placeholder="Input" ref={register}/>
                    </label>
                    <label className="halfwidth" >
                        resourceTypeGeneral:
                    <select name="elements[0].elements[5].attributes.resourceTypeGeneral"  ref={register}>
                        <option value="">[resourceTypeGeneral] </option>
                        <option value="Audiovisual">Audiovisual</option>
                        <option value="Collection">Collection</option>
                        <option value="Dataset">Dataset</option>
                        <option value="Image">Image</option>
                        <option value="Model">Model</option>
                        <option value="Software">Software</option>
                        <option value="Sound">Sound</option>
                        <option value="Text">Text</option>
                        <option value="Workflow">Workflow</option>
                        <option value="Other">Other</option>
                    </select>
                    </label>
                </label>
            </fieldset>

        </>
    )
}