import React from "react";






export default function Sizes({register}){


    const [sizes, setSizes] = React.useState([0]);
    const [counter, setCounter] = React.useState(1);

    const addSizesFieldset = () => {
        setSizes(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };


    const removeSizesFieldset = index => () => {
        setSizes(prevIndexes => [...prevIndexes.filter(item => item !== index)]);
        setCounter(prevCounter => prevCounter - 1);
    }
    return (
        <>
            <fieldset>
                <label> Sizes:
                    <input type="hidden" name="elements[0].elements[14].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[14].name" defaultValue="sizes" ref={register}/>
                    {sizes.map(index => {
                        const sizesFieldName = `elements[0].elements[14]elements[${index}]`;
                        return (
                            <fieldset name={sizesFieldName} key={sizesFieldName} >
                                <input type="hidden" name={`${sizesFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${sizesFieldName}.name`} defaultValue="size" ref={register}/>
                                <label>Size:
                                    <input type="hidden" name={`${sizesFieldName}.elements[0].type`} defaultValue="text" ref={register}/>
                                    <input name={`${sizesFieldName}.elements[0].text`} placeholder="Input" ref={register}/>
                                </label>

                                <button type="button" onClick={addSizesFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeSizesFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}

                </label>
            </fieldset>

        </>
    )
}