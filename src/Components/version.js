import React from "react";

export default function Version({register}) {
    return (
        <>

            <fieldset>
                <label> Version :
                    <input type="hidden" name="elements[0].elements[16].type" defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[16].name" defaultValue="version" ref={register}/>
                    <input type="hidden" name="elements[0].elements[16].elements[0].type" defaultValue="text" ref={register}/>
                    <input name="elements[0].elements[16].elements[0].text" placeholder="Input" ref={register}/>
                </label>
            </fieldset>

        </>
    )
}