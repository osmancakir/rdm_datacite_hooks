import React from "react";



export default function RightsList({register}) {

    // dynamic input

    const [counter, setCounter] = React.useState(1);
    const [rightsList, setRightsList] = React.useState([0]);

    const addRightsListFieldset = () => {
        setRightsList(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };

    const removeRightsListFieldset = index => () => {
        setRightsList(prevIndexes => [
            ...prevIndexes.filter(item => item !== index)
        ]);
        setCounter(prevCounter => prevCounter - 1);
    };



    return(
        <>
            <fieldset>
                <label> Rights List:
                    <input type="hidden" name="elements[0].elements[17].type"  defaultValue="element" ref={register}/>
                    <input type="hidden" name="elements[0].elements[17].name" defaultValue="rightsList" ref={register}/>

                    {rightsList.map(index => {
                        const rightsListFieldName = `elements[0].elements[17].elements[${index}]`;
                        return (
                            <fieldset name={rightsListFieldName} key={rightsListFieldName} >
                                <input type="hidden" name={`${rightsListFieldName}.type`} defaultValue="element" ref={register}/>
                                <input type="hidden" name={`${rightsListFieldName}.name`} defaultValue="rights" ref={register}/>

                                <label> Rights:
                                    <input type="hidden" name={`${rightsListFieldName}.elements[0].type`}  ref={register}/>
                                    <input  name={`${rightsListFieldName}.elements[0].text`}  placeholder="Input" ref={register}/>
                                </label>

                                <label> Language:
                                    <input className="language" name={`${rightsListFieldName}.attributes.xml:lang`} placeholder="Input" ref={register}/>
                                </label>

                                <label > schemeURI:
                                    <select  name={`${rightsListFieldName}.attributes.schemeURI`} ref={register}>
                                        <option value="">[schemeURI]</option>
                                        <option value="GND">GND</option>
                                        <option value="wikidata">wikidata</option>
                                        <option value="dewey">dewey</option>
                                    </select>
                                </label>

                                <label> rightsIdentifierScheme:
                                    <input name={`${rightsListFieldName}.attributes.rightsIdentifierScheme`} placeholder="Input" ref={register}/>
                                </label>

                                <label className ="halfwidth" > rightsIdentifier:
                                    <input name={`${rightsListFieldName}.attributes.rightsIdentifier`} placeholder="Input" ref={register}/>
                                </label>
                                <label className ="halfwidth" > rightsURI:
                                    <input name={`${rightsListFieldName}.attributes.rightsURI`} placeholder="Input" ref={register}/>
                                </label>


                                <button type="button" onClick={addRightsListFieldset}>
                                    +
                                </button>
                                <button type="button" onClick={removeRightsListFieldset(index)}>
                                    -
                                </button>
                            </fieldset>
                        );
                    })}
                </label>
            </fieldset>
        </>
    )
}