import React from "react";

// <pre>{JSON.stringify(state, null, 2)}</pre>

export default ({ prev, convertJSON, convertXml }) => {


    return (
        <div className="container">
            <h2>Result</h2>
            <button type="button"
                    className="appButton"
                    onClick={convertXml}
            >
                convert xml
            </button>
            <button type="button" disabled="true"
                    className="appButton"
                    onClick={convertJSON}
            >
                convert JSON
            </button>
            <button type="button"
                    className="appButton"
                    onClick={prev}>
                Previous
            </button>



        </div>
    );
};
